package com.packiyo.assignment.dto;


import lombok.Data;

import java.time.LocalDate;

@Data
public class QueryData {
    private String country;
    private String city;
    private LocalDate date;
}
