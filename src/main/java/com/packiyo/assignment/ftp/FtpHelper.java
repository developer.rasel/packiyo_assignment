package com.packiyo.assignment.ftp;

import com.packiyo.assignment.utils.Utils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class FtpHelper {
    private static final Logger LOGGER = Logger.getLogger(FtpHelper.class.getName());
    @Value("${ftp.server.host}")
    private  String FTP_SERVER;
    @Value("${ftp.server.port}")
    private  int FTP_PORT;
    @Value("${ftp.server.user}")
    private  String FTP_USERNAME;
    @Value("${ftp.server.password}")
    private String FTP_PASSWORD;
    @Value("${ftp.server.xml.path}")
    private  String FTP_BASE_PATH;
    @Value("${ftp.server.file.path}")
    private String remoteFilePath;
    @Autowired
    private Utils utils;

    public Map<String, List<String>> retrieveDataFromFtp() {
        Map<String, List<String>> ftpRecords = new HashMap<>();
        ftpRecords.put("countries", new ArrayList<>());
        ftpRecords.put("cities", new ArrayList<>());
        ftpRecords.put("dates", new ArrayList<>());

        try {
            FTPClient ftpClient = new FTPClient();
            ftpClient.connect(FTP_SERVER, FTP_PORT);
            ftpClient.login(FTP_USERNAME, FTP_PASSWORD);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            try (InputStream inputStream = ftpClient.retrieveFileStream(remoteFilePath);
                 InputStreamReader reader = new InputStreamReader(inputStream);
                 CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader())) {

                for (CSVRecord record : csvParser) {
                    String country = record.get("Country");
                    String city = record.get("City");
                    String date = record.get("Date");

                    ftpRecords.get("countries").add(country);
                    ftpRecords.get("cities").add(city);
                    ftpRecords.get("dates").add(date);

                    LOGGER.log(Level.INFO, "parsed output from ftp file:" + ftpRecords);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.log(Level.INFO, "could not parsed ftp file:" + e.getMessage());
        }
        return ftpRecords;
    }

    public void uploadXmlToFTP(String fileName, String xmlContent) {
        fileName = utils.removeSpecialChar(fileName);
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(FTP_SERVER, FTP_PORT);
            ftpClient.login(FTP_USERNAME, FTP_PASSWORD);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            InputStream inputStream = new ByteArrayInputStream(xmlContent.getBytes(StandardCharsets.UTF_8));

            boolean success = ftpClient.storeFile(FTP_BASE_PATH + fileName, inputStream);
            if (success) {
                LOGGER.log(Level.INFO, "File " + fileName + " uploaded successfully to FTP server.");
            } else {
                LOGGER.log(Level.SEVERE, "Failed to upload file " + fileName + " to FTP server.");

            }
            inputStream.close();
            ftpClient.logout();
            ftpClient.disconnect();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Exception during upload file :" + e.getMessage());
        }
    }
}
