package com.packiyo.assignment.config;

import com.packiyo.assignment.services.DataProcessor;
import com.packiyo.assignment.ftp.FtpHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class Worker {
    private static final Logger LOGGER = Logger.getLogger(Worker.class.getName());
    @Autowired
    DataProcessor dataProcessor;
    @Autowired
    FtpHelper ftpHelper;

    @Scheduled(fixedRate = 600_000) // 10 minutes = 600,000 milliseconds
    public void runScheduledTask() {
        LOGGER.log(Level.INFO, "Job Running Notify Current Time: " + LocalDateTime.now());
        HashMap<String, List<String>> ftpPersedMap = (HashMap<String, List<String>>) ftpHelper.retrieveDataFromFtp();
        if (ftpPersedMap!=null){
            List<String> cityList = (List<String>) ftpPersedMap.get("cities");
            if (cityList.size()>0){
                for (String city : cityList) {
                    dataProcessor.processCountryData(city);
                }
            }else {
                LOGGER.log(Level.INFO, "city size is:" + cityList.size());
            }
        }else {
            LOGGER.log(Level.INFO, "parsed ftp file is null!");
        }



    }
}
