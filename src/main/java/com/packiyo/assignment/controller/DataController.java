package com.packiyo.assignment.controller;

import com.packiyo.assignment.dto.QueryData;
import com.packiyo.assignment.ftp.FtpHelper;
import com.packiyo.assignment.services.DataProcessor;
import com.packiyo.assignment.services.RemoteDataReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
@RequestMapping(value = "/api/v1")
public class DataController {
    private static final Logger LOGGER = Logger.getLogger(FtpHelper.class.getName());
    @Autowired
    private FtpHelper ftpHelper;
    @Autowired
    private RemoteDataReader remoteDataReader;
    @Autowired
    private DataProcessor dataProcessor;

    @RequestMapping(value = "/country", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getCities() {
        LOGGER.log(Level.INFO, "call country api");
        HashMap<String, List<String>> ftpData = (HashMap<String, List<String>>) ftpHelper.retrieveDataFromFtp();
        LOGGER.log(Level.INFO, "response from country api:"+ftpData.get("countries"));
        return ftpData.get("countries");
    }

    @RequestMapping(value = "/query", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getQueryData( @RequestBody QueryData queryData) {
        LOGGER.log(Level.INFO, "call data query api request body:"+queryData );
        Map<String, Object> data = new HashMap<>();
        data.put("country", queryData.getCountry());
        data.put("hotels", remoteDataReader.getHotelsData(queryData.getCountry(), queryData.getCity(), String.valueOf(queryData.getDate())));
        data.put("weather", remoteDataReader.getWeatherData(queryData.getCountry(), queryData.getCity(), String.valueOf(queryData.getDate())));
        LOGGER.log(Level.INFO, "response from query data api:"+data);
        return data;
    }

    @RequestMapping(value = "/export", method = RequestMethod.POST)
    @ResponseBody
    public String exportData( @RequestBody QueryData queryData) {
        LOGGER.log(Level.INFO, "call export data api request body:"+queryData );
        Map<String, Object> data = new HashMap<>();
        data.put("country", queryData.getCountry());
        data.put("hotels", remoteDataReader.getHotelsData(queryData.getCountry(), queryData.getCity(), String.valueOf(queryData.getDate())));
        data.put("weather", remoteDataReader.getWeatherData(queryData.getCountry(), queryData.getCity(), String.valueOf(queryData.getDate())));
        String xmlContent = dataProcessor.convertToXml(data);
        ftpHelper.uploadXmlToFTP(queryData.getCountry() + ".xml", xmlContent);
        return "success";
    }
}
