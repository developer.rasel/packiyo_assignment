package com.packiyo.assignment.utils;

import com.packiyo.assignment.ftp.FtpHelper;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class Utils {
    private static final Logger LOGGER = Logger.getLogger(FtpHelper.class.getName());
    public String removeSpecialChar(String input){
        String result = input.replaceAll("[^a-zA-Z0-9 .]", "");
        LOGGER.log(Level.INFO, "after remove special char output: ",result);
        return result;
    }

}
