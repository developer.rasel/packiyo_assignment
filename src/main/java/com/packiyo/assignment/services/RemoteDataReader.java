package com.packiyo.assignment.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.packiyo.assignment.utils.Utils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class RemoteDataReader {
    private static final Logger LOGGER = Logger.getLogger(RemoteDataReader.class.getName());

    @Value("${hotel.api.url}")
    private String HOTELS_API_URL;
    @Value("${weather.api.url}")
    private String WEATHER_API_URL;
    @Value("${hotel.api.host}")
    private String HOTELS_API_HOST;
    @Value("${hotel.api.key}")
    private String HOTELS_API_KEY;
    @Value("${weather.api.key}")
    private String WEATHER_API_KEY;
    @Autowired
    private Utils utils;


    public Map<String, Object> getHotelsData(String country, String city, String date) {
        Map<String, Object> hotelsResponse = new HashMap<>();
        try {
            String encodedCity = URLEncoder.encode(city, StandardCharsets.UTF_8.toString());
            String apiUrl = HOTELS_API_URL + "?query=" + encodedCity + "&locale=en_US";
            if (country != null && date != null) {
                String encodedCountry = URLEncoder.encode(country, StandardCharsets.UTF_8.toString());
                apiUrl = HOTELS_API_URL + "?query=" + encodedCity + "&locale=en_US&country=" + encodedCountry + "&date=" + date;
            }

            HttpClient client = HttpClients.createDefault();
            HttpGet request = new HttpGet(apiUrl);
            request.addHeader("X-RapidAPI-Host", HOTELS_API_HOST);
            request.addHeader("X-RapidAPI-Key", HOTELS_API_KEY);

            HttpResponse response = client.execute(request);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuilder hotelsData = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                hotelsData.append(line);
            }

            ObjectMapper mapper = new ObjectMapper();
            hotelsResponse = mapper.readValue(hotelsData.toString(), new TypeReference<Map<String, Object>>() {
            });

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hotelsResponse;
    }

    public Map<String, Object> getWeatherData(String country, String city, String date) {
        Map<String, Object> weatherResponse = new HashMap<>();
        try {
            String encodedCity = URLEncoder.encode(city, StandardCharsets.UTF_8.toString());
            String apiUrl = WEATHER_API_URL + "?q=" + encodedCity + "&appid=" + WEATHER_API_KEY;
            if (country != null && date != null) {
                String encodedCountry = URLEncoder.encode(country, StandardCharsets.UTF_8.toString());
                apiUrl = WEATHER_API_URL + "?q=" + encodedCity + "&country=" + encodedCountry + "&date=" + date + "&appid=" + WEATHER_API_KEY;
            }

            HttpClient client = HttpClients.createDefault();
            HttpGet request = new HttpGet(apiUrl);

            HttpResponse response = client.execute(request);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuilder weatherData = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                weatherData.append(line);
            }
            ObjectMapper mapper = new ObjectMapper();
            weatherResponse = mapper.readValue(weatherData.toString(), new TypeReference<Map<String, Object>>() {
            });
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return weatherResponse;
    }
}
