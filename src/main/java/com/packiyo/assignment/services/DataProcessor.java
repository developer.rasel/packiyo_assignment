package com.packiyo.assignment.services;

import com.packiyo.assignment.ftp.FtpHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class DataProcessor {
    private static final Logger LOGGER = Logger.getLogger(FtpHelper.class.getName());
    @Autowired
    private RemoteDataReader remoteDataReader;
    @Autowired
    private FtpHelper ftpHelper;

    public void processCountryData(String city) {
        Map<String, Object> data = new HashMap<>();
        data.put("city", city);
        data.put("hotels", remoteDataReader.getHotelsData(null, city, null));
        data.put("weather", remoteDataReader.getWeatherData(null, city, null));
        String xmlContent = convertToXml(data);
        ftpHelper.uploadXmlToFTP(city + ".xml", xmlContent);
    }

    public String convertToXml(Map<String, Object> data) {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            Element rootElement = doc.createElement("Data");

            Element countryElement = doc.createElement("Country");
            countryElement.appendChild(doc.createTextNode((String) data.get("country")));
            rootElement.appendChild(countryElement);

            Element hotelsElement = doc.createElement("Hotels");
            Map<String, Object> hotelsData = (Map<String, Object>) data.get("hotels");
            for (Map.Entry<String, Object> entry : hotelsData.entrySet()) {
                Element hotelElement = doc.createElement("Hotel");
                hotelElement.setAttribute("name", entry.getKey());
                hotelElement.appendChild(doc.createTextNode(String.valueOf(entry.getValue())));
                hotelsElement.appendChild(hotelElement);
            }
            rootElement.appendChild(hotelsElement);

            Element weatherElement = doc.createElement("Weather");
            Map<String, Object> weatherData = (Map<String, Object>) data.get("weather");
            for (Map.Entry<String, Object> entry : weatherData.entrySet()) {
                Element weatherInfoElement = doc.createElement("WeatherInfo");
                weatherInfoElement.setAttribute("date", entry.getKey());
                weatherInfoElement.appendChild(doc.createTextNode(String.valueOf(entry.getValue())));
                weatherElement.appendChild(weatherInfoElement);
            }
            rootElement.appendChild(weatherElement);

            doc.appendChild(rootElement);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));

            return writer.getBuffer().toString();
        } catch (ParserConfigurationException | TransformerException e) {
            LOGGER.log(Level.SEVERE, "Exception occurred while combining data", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
}
