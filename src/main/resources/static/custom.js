$(document).ready(function() {
    // Make AJAX request to fetch cities
    var contextPath = window.location.origin;
    $.ajax({

        url: contextPath+"/api/v1/country",
        method: "GET",
        dataType: "json",
        success: function(response) {
            // Populate the dropdown with country options
            var cityDropdown = $("#country"); // Replace "countryDropdown" with the actual ID of your dropdown element
            cityDropdown.append($("<option>").text("Select a country"));
            $.each(response, function(index, country) {
                cityDropdown.append($("<option>").val(country).text(country));
            });
        },
        error: function(xhr, status, error) {
            console.log("Error fetching countries:", error);
        }
    });
});


    function getData() {
        $('#loader').show();
        var country = $('#country').val();
        var city = $('#city').val();
        var date = $('#date').val();

        // Create an object with the data to send in the request body
        var queryData = {
            country: country,
            city: city,
            date: date
        };

        // Make the AJAX POST request
        var contextPath = window.location.origin;
        $.ajax({
            url: contextPath+'/api/v1/query', // Replace with the actual API endpoint URL
            type: 'POST',
            data: JSON.stringify(queryData),
            contentType: 'application/json',
            success: function(response) {
                debugger;
                // Handle the success response and populate the table
                populateTable(response);
                $('#exp-btn').show();
                $('#loader').hide();
            },
            error: function(error) {
                // Handle the error response
                console.error(error);
                $('#loader').hide();
            }
        });
    }

    function populateTable(data) {
        var table = $('<table>').addClass('table');
        var thead = $('<thead>');
        var tbody = $('<tbody>');

        // Create table header
        var headerRow = $('<tr>');
        headerRow.append($('<th>').text('Country'));
        headerRow.append($('<th>').text('Hotels'));
        headerRow.append($('<th>').text('Weather'));
        thead.append(headerRow);

        // Create table rows
        var row = $('<tr>');
        row.append($('<td>').text(JSON.stringify(data.country)));
        row.append($('<td>').text(JSON.stringify(data.hotels)));
        row.append($('<td>').text(JSON.stringify(data.weather)));
        tbody.append(row);

        // Append the table header and rows to the table
        table.append(thead);
        table.append(tbody);
        var tableContainer = $('<div>').addClass('table-responsive');
        tableContainer.append(table);
        // Clear the existing data container and append the table
        $('#data-container').empty().append(table);
    }

    function exportData(){
        $('#loader').show();
        var country = $('#country').val();
        var city = $('#city').val();
        var date = $('#date').val();

        var queryData = {
            country: country,
            city: city,
            date: date
        };
        var contextPath = window.location.origin;
        $.ajax({
            url: contextPath+'/api/v1/export', // Replace with the actual API endpoint URL
            type: 'POST',
            data: JSON.stringify(queryData),
            contentType: 'application/json',
            success: function(response) {
                $('#loader').hide();
                alert("Successfully export data into server!");
            },
            error: function(error) {
                // Handle the error response
                console.error(error);
                $('#loader').hide();
                alert("failed to export data into server!");
            }
        });
    }


